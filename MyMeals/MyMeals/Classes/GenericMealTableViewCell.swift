//
//  GenericMealTableViewCell.swift
//  MyMeals
//
//  Created by Henri LA on 11.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class GenericMealTableViewCell: UITableViewCell {
    
    //#warning("To complete the implementation")
    // TODO: Link the UIComponents from the xib
    @IBOutlet private weak var genericImageView: UIImageView!
    @IBOutlet private weak var genericTitleLabel: UILabel!
    @IBOutlet private weak var genericSubtitle: UILabel!
    
    private var imageIdentifier: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Clean values before first usage - some values may have been set in the xib
        clearValues()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // Clean values before reused
        clearValues()
    }
    
    private func clearValues(){
        genericImageView.image = nil
        genericTitleLabel.text = nil
        genericSubtitle.text = nil
        imageIdentifier = nil
        genericImageView.backgroundColor = UIColor.clear
    }
    
    // MARK: Configurations
    
    func configCellForModel(_ model: MealCategory, with imageStore: ImageStore) {
        //#warning("To complete the implementation")
        // TODO: Link the corresponding model to the UIComponents
        genericTitleLabel.text = model.strCategory
        genericSubtitle.text = model.strCategoryDescription
        self.configureImageForURL(model.imageURL!, and:imageStore)
    }
    
    func configCellForModel(_ model: Meal, with imageStore: ImageStore) {
        //#warning("To complete the implementation")
        // TODO: Link the corresponding model to the UIComponents
        genericTitleLabel.text = model.strMeal
        self.configureImageForURL(model.imageURL!, and:imageStore)
        
    }
    
    func configCellForModel(_ model: MealDetails, with imageStore: ImageStore) {
        //#warning("To complete the implementation")
        // TODO: Link the corresponding model to the UIComponents
        genericTitleLabel.text = model.strMeal
        genericSubtitle.text = model.strCategory
        self.configureImageForURL(model.imageURL!, and:imageStore)
    }
    
    // MARK: Helper
    
    private func configureImageForURL(_ imageURL: URL, and imageStore: ImageStore) {
        // Set the imageIdentifier
        imageIdentifier = imageURL.absoluteString
        
        // Load the image from the url
        imageStore.loadImageFromURL(imageURL, for: imageURL.absoluteString) { [weak self] (image, identifier) in
            // Check if we have an image AND if the imageIdentifier is matching with identifier
            guard let image = image, self?.imageIdentifier == identifier else {
                return
            }
            
            // If it is fine, just assign it !
            DispatchQueue.main.async {
                self?.genericImageView?.image = image
            }
        }
    }
}

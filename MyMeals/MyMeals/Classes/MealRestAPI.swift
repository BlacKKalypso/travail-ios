//
//  MealRestAPI.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation
import CoreData

typealias MealCategoriesResponse = (mealCategories: [MealCategory], error: Error?)
typealias MealResponse = (meals: [Meal], error: Error?)
typealias MealDetailsResponse = (mealDetails: MealDetails?,error: Error?)
typealias SearchedMealsDetailsResponse = (arrayMealDetails: [MealDetails], error: Error?)

class MealRestAPI {
  
  // MARK: Variables configuration
  
  private let restEndpoint: RestEndpoint
  private let networkOperator: NetworkOperator
  private let managedObjectContext: NSManagedObjectContext?
  
  // MARK: Init
  
  init(restEndpoint: RestEndpoint,
       networkOperator: NetworkOperator,
       managedObjectContext: NSManagedObjectContext?)
  {
    self.restEndpoint = restEndpoint
    self.networkOperator = networkOperator
    self.managedObjectContext = managedObjectContext
  }
  
  // MARK: Fetch data
  
  func fetchMealCategories( completion: @escaping (MealCategoriesResponse) -> Void ) {
    networkOperator.sendRequest(restEndpoint.requestMealCategories()) { (responseResult) in
      completion(MealCategory.mealCategories(from: responseResult))
    }
  }
  
  func fetchMealsForCategoryName(_ categoryName: String, completion: @escaping (MealResponse) -> Void ) {
    networkOperator.sendRequest(restEndpoint.requestMeals(for: categoryName)) { (responseResult) in
      completion(Meal.meals(from: responseResult))
    }
  }
  
  func fetchMealDetailsForMealID(_ mealID: String, completion: @escaping (MealDetailsResponse) -> Void ) {
    networkOperator.sendRequest(restEndpoint.requestMealDetails(for: mealID)) { (responseResult) in
      completion(MealDetails.meal(from: responseResult))
    }
  }
  
  func fetchArrayOfMealDetailsForMealName(_ mealName: String,
                                          completion: @escaping (SearchedMealsDetailsResponse) -> Void ) -> URLSessionDataTask
  {
    return networkOperator.sendRequest(restEndpoint.requestSearchMeal(for: mealName)) { (responseResult) in
      completion(MealDetails.meals(from: responseResult))
    }
  }
  
}


private extension RestEndpoint {
  
  func requestMealCategories() -> URLRequest {
    guard let url = self.url(forPath: "/categories.php", andQueryItems: nil) else {
      fatalError("Incorrect url")
    }
    
    return URLRequest(url: url)
  }
  
  func requestMeals(for categoryName: String) -> URLRequest {
    guard let url = self.url(forPath: "/filter.php?c="+categoryName, andQueryItems: nil) else{
        fatalError("Incorrect url")  // Remove this line when you have implemented the func
    }
    // Hint: Try to understand requestMealCategories to make it works
    return URLRequest(url: url)
  }
  
  func requestMealDetails(for mealID: String) -> URLRequest {
    // Hint: Try to understand requestMealCategories to make it works
    guard let url = self.url(forPath: "/lookup.php?i="+mealID, andQueryItems: nil) else {
     fatalError("Incorrect id") // Remove this line when you have implemented the func
    }
    
    return URLRequest(url: url)
  }
  
  func requestSearchMeal(for mealName: String) -> URLRequest {
    //#warning("To complete the implementation")
    // Hint: Try to understand requestMealCategories to make it works
    guard let url = self.url(forPath: "/search.php?s="+mealName, andQueryItems: nil) else {
        fatalError("Meal not found") // Remove this line when you have implemented the func
    }
    
    return URLRequest(url: url)
  }
  
}

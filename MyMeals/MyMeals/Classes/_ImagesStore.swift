//
//  ImagesStore.swift
//  Try!
//
//  Created by Henri LA on 08.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class ImageStore {
  
  let cache: NSCache<NSString, UIImage>?
  let session: URLSession
  
  init(with session: URLSession = URLSession(configuration: .default),
       and cache: NSCache<NSString, UIImage>? = nil) {
    self.session = session
    self.cache = cache
  }
  
  @discardableResult
  func loadImageFromURL(_ url: URL,for identifier: String, completion: @escaping (UIImage?, String) -> () ) -> URLSessionDataTask? {
    
    // Get image from cache if available
    if let image = cache?.object(forKey: identifier as NSString) {
      completion(image, identifier)
      return nil
    }
    
    let dataTask = session.dataTask(with: url) { [weak self] (data, response, error) in
      guard let data = data else {
        completion(nil, identifier)
        return
      }
      
      // Improvement: Handle response/error
      
      guard let image = UIImage(data: data) else {
        completion(nil, identifier)
        return
      }
      
      // Update the cache with the latest image
      self?.cache?.setObject(image, forKey: identifier as NSString)
      
      completion(image, url.absoluteString)
    }
    
    dataTask.resume()
    
    return dataTask
  }
  
}

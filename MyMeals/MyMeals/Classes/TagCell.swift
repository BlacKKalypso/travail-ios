//
//  TagCell.swift
//  Meals
//
//  Created by Henri LA on 05.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {

  @IBOutlet private weak var titleLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    titleLabel.text = nil
  }
  
  func setTitleLabel(with text: String?){
    titleLabel.text = text
  }

}

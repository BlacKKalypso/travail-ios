//
//  MealCategoriesDataSource.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

class MealCategoriesDataSource: TableViewDataSourceQuickSearch {
    typealias T = MealCategory
    
    // MARK: Variables configuration
    
    private let mealRestAPI: MealRestAPI
    private var reloadDelegate: GenericDataSourceReloadable?
    
    // MARK: Private Variables
    
    private var dataSource = [String: [MealCategory]]()
    private var headerSections = [String]()
    
    // MARK: Inits
    
    init(mealRestAPI: MealRestAPI, reloadDelegate: GenericDataSourceReloadable){
        self.mealRestAPI = mealRestAPI
        self.reloadDelegate = reloadDelegate
    }
    
    private func formatMealCategories(_ mealCategories: [MealCategory]) {
        //#warning("To complete the implementation")
        // TODO: Reset dataSource
        dataSource = ["": []]
        // TODO: Reset headerSections
        headerSections.removeAll()
        
        
        // TODO: Sort the mealCategories by name in alphabetic order - On an array, use the func .sorted..
        let orderedCategory = mealCategories.sorted{ $0.strCategory < $1.strCategory }
        
        // TODO: Fill headerSections - headerSections is an [String] and each item contains only a letter !
        // The first character of a string : .first
        // If there is no value just ignore this category !
        for meal in orderedCategory{
            let indexCategory = meal.strCategory.prefix(1)

            headerSections.append(String(indexCategory))
        }
        
//        let firstLetterArray = dataSource.map { "\(String(describing: $0.value.first))" }
//        headerSections = firstLetterArray
//        print(headerSections)
        
    }
    
    // MARK: TableViewDataSourceQuickSearch implementation
    
    func reload() {
        mealRestAPI.fetchMealCategories { [weak self] (mealCategories, error) in
            self?.formatMealCategories(mealCategories)
            self?.reloadDelegate?.dataSourceDidReload(with: error)
        }

    }
    
    func numberOfSections() -> Int {
        return headerSections.count
    }
    
    func numberOfItems(in section: Int) -> Int {
        let key = headerSections[section]
        guard let total = dataSource[key]?.count else {
            return 0
        }
        
        return total
    }
    
    func item(at indexPath: IndexPath) -> MealCategory? {
        let key = headerSections[indexPath.section]
        return dataSource[key]?[indexPath.row]
    }
    
    func sectionIndexTitles() -> [String]? {
        return headerSections
    }
    
    func titleForSection(_ section: Int) -> String? {
        return headerSections[section]
    }
    
}

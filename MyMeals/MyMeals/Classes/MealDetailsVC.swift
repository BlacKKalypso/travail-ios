//
//  MealDetailsVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealDetailsVC: UIViewController {

  // MARK: UIComponents
  
  #warning("Link the UIComponents below from you xib")
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var categoryTitleLabel: UILabel!
  @IBOutlet private weak var categoryValueLabel: UILabel!
  @IBOutlet private weak var areaTitleLabel: UILabel!
  @IBOutlet private weak var areaValueLabel: UILabel!
  @IBOutlet private weak var lastTimeSeenTitleLabel: UILabel!
  @IBOutlet private weak var lastTimeSeenValueLabel: UILabel!
  @IBOutlet private weak var tagsCollectionView: UICollectionView!
  @IBOutlet private weak var instructionsLabel: UILabel!
  
  private let mealDetails: MealDetails
  
  // MARK: Inits
  
  init(mealDetails: MealDetails) {
    self.mealDetails = mealDetails
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    #warning("To complete the implementation")
    // TODO: Set the title from mealDetails by using strMeal
    
    // TODO: Add an UIBarButtonItem to navigationItem.setLeftBarButton(...)
    // - By clicking on the UIBarButton, the ViewController will "dismiss"
    // - In order to make it works, you need to use the func closeVC()
    
    
    
    // TODO: Configure the ViewController by adding all data from MealDetails to your UIComponent !
  }
  
  // MARK: Action(s)
  
  @objc private func closeVC(){
    // Dimiss the modal viewController
    dismiss(animated: true, completion: nil)
  }
  
}

// MARK: Bonus!

extension MealDetailsVC: UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    #warning("To complete the implementation")
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    #warning("To complete the implementation")
    return UICollectionViewCell()
  }
  
}

extension MealDetailsVC: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    #warning("To complete the implementation")
    return CGSize()
  }
  
}



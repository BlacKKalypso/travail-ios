//
//  NetworkOperator.swift
//  Meals
//
//  Created by Henri LA on 03.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct ResponseResult {
  let data: Data?
  let urlResponse: URLResponse?
  let error: Error?
}

class NetworkOperator {
  
  // MARK: Variables configuration
  
  private let sessionConfiguration: URLSessionConfiguration
  private let operationQueue: OperationQueue
  private let urlCache: URLCache
  
  private lazy var urlSession: URLSession = {
     let session = URLSession(configuration: sessionConfiguration,
                              delegate: nil,
                              delegateQueue: operationQueue)
    return session
  }()
  
  // MARK: Init
  
  init(_ sessionConfiguration: URLSessionConfiguration,
       operationQueue: OperationQueue,
       cache: URLCache)
  {
    self.sessionConfiguration = sessionConfiguration
    self.operationQueue = operationQueue
    self.urlCache = cache
  }
  
  // MARK: Functions
  
  @discardableResult
  func sendRequest(_ urlRequest: URLRequest,
                   completion: @escaping (ResponseResult) -> () ) -> URLSessionDataTask
  {
    let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
      let result = ResponseResult(data: data, urlResponse: urlResponse, error: error)
      completion(result)
    }
    dataTask.resume()
    
    return dataTask
  }
  
}

//
//  ModalFullScreenNavigationVC.swift
//  MyMeals
//
//  Created by Henri LA on 10.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class ModalFullScreenNavigationVC: UINavigationController {
  
  override init(rootViewController: UIViewController) {
    super.init(rootViewController: rootViewController)
    modalPresentationStyle = .fullScreen
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

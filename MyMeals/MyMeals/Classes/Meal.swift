//
//  Meal.swift
//  Meals
//
//  Created by Henri LA on 05.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import Foundation

struct Meal: Codable {
    
    let strMeal: String?
    let strMealThumb: String?
    let idMeal: String
    
    init(_ dictionary: [String: String] ){
        idMeal = dictionary["idMeal"] ?? ""
        strMeal = dictionary["strMeal"]
        strMealThumb = dictionary["strMealThumb"]
    }
    
    var imageURL: URL? {
        let result: URL?
        if let thumbStringURL = strMealThumb{
            result = URL(string: thumbStringURL)
        } else {
            result = nil
        }
        
        return result
    }
    
}

extension Meal {
    
    static func meals(from responseResult: ResponseResult) -> MealResponse {
        var result: MealResponse
        result.meals = []
        result.error = nil
        
        // TODO: Set error
        result.1 = responseResult.error
        
        // TODO: Set result.meals -> it expects a [Meal]
        if let data = responseResult.data,
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
            let meals = json["meals"] as? [[String: String]]
        {
            var dataResult = [Meal]()
            
            meals.forEach{
                dataResult.append(Meal($0)) }
            
            result.meals = dataResult
        }
        
        return result
    }
    
}

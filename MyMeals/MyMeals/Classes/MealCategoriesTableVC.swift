//
//  MealCategoriesTableVC.swift
//  MyMeals
//
//  Created by Henri LA on 09.12.19.
//  Copyright © 2019 Crea. All rights reserved.
//

import UIKit

class MealCategoriesTableVC: UITableViewController {
    
    
    // MARK: Variables configuration
    private lazy var mealCategoriesDataSource = NoIndexMealCategoriesDataSource(mealRestAPI: UIApplication.appDelegate.mealRestAPI,                                                            reloadDelegate: self)
    //#warning("When MealCategoriesDataSource is fixed - comment the line above and uncomment the line below")
    //    private lazy var mealCategoriesDataSource = MealCategoriesDataSource(mealRestAPI: UIApplication.appDelegate.mealRestAPI, reloadDelegate: self)
    // MARK: Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        //#warning("To complete the implementation")
        // TODO: Localized the title
        title = NSLocalizedString("_meal_categories_tablevc_title_", comment: "")
        
        tabBarItem = UITabBarItem(title: title, image: UIImage(named: "tab1"), tag: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //#warning("To complete the implementation")
        // TODO: Register the tableViewCell you created
        let reuseIndentifier = GenericMealTableViewCell.reuseIdentifier
        let nibMealCell = UINib(nibName: reuseIndentifier, bundle: Bundle.main)
        tableView.register(nibMealCell, forCellReuseIdentifier: reuseIndentifier)
        
        mealCategoriesDataSource.reload()
    }
    
    // MARK: TableView data source & delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        //#warning("To complete the implementation")
        // TODO: Complete the missing implementation
        return mealCategoriesDataSource.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //#warning("To complete the implementation")
        // TODO: Complete the missing implementation
        return mealCategoriesDataSource.numberOfItems(in: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //#warning("Correct this implementation")
        // TODO: Complete the incorrect implementation in order to return a configured cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenericMealTableViewCell", for: indexPath)
        
        if let mealCategoryCell = cell as? GenericMealTableViewCell,
            let mealCategory = mealCategoriesDataSource.item(at: indexPath) {
            mealCategoryCell.configCellForModel(mealCategory, with: ImageStore())
        }
        
        return cell
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        //#warning("To complete the implementation")
        // TODO: Complete the implementation in order to have a quick index
        
        // It returns an array of letters
        return mealCategoriesDataSource.sectionIndexTitles()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //#warning("To complete the implementation")
        // TODO: Complete the missing implementation
        return mealCategoriesDataSource.titleForSection(section)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //#warning("To complete the implementation")
        // TODO: Complete the missing implementation
        /*
         1. When selecting, it push a MealListVC.
         2. Deselct the cell that was selected.
         */
         let genericMeal = MealsTableVC(mealCategory: mealCategoriesDataSource.item(at: indexPath)!)
          navigationController?.pushViewController(genericMeal, animated: true)
          tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

// MARK: GenericDataSourceReloadable implementation for MealCategoriesTableVC

extension MealCategoriesTableVC: GenericDataSourceReloadable {
    
    func dataSourceDidReload(with error: Error?) {
        //#warning("To complete the implementation")
        // TODO: Show an UIAlertViewController if there is an error
        // The title should be localized -> Check the Localizable.string for the title
        // The message should be set with error.localizedDescription
        // An UIAlertAction needs to be added your UIAlertViewController.
        // The UIAlertAction should be also localized and should be of style cancel
        // To present it, use present(...) by giving the instance of UIAlertViewController
        if let error = error {
        let alert = UIAlertController(title: NSLocalizedString("_alertViewController_title_", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("_alertViewController_ok_", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
        tableView.reloadData()
    }
    
}
